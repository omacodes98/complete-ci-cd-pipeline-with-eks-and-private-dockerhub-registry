# Demo Project - Complete CI/CD Pipeline with EKS and private DockerHub registry 

## Table of Contents

- [Project Description](#project-description)

- [Technologies Used](#technologies-used)

- [Steps](#steps)

- [Usage](#usage)

- [How to Contribute](#how-to-contribute)

- [Questions](#questions)

- [References](#references)

- [License](#license)

## Project Description

* Write K8s manifest files for Deployment and Service configuration

* Integrate deploy step in the CI/CD pipeline to deploy newly built application image from DockerHub private registry to the EKS cluster

* The complete CI/CD project has the following configuration:

a. CI step: Increment version

b. CI step: Build artifact for Java Maven application

c. CI step: Build and push Docker image to DockerHub

d. CD step: Deploy new application version to EKS cluster 

e. CD step: Commit the version update

## Technologies Used 

* Kubernetes 

* Jenkins 

* AWS EKS 

* Docker Hub 

* Java 

* Maven 

* Linux 

* Docker 

* Git 

## Steps 

Step 1: Create Deployment manifest file for Jenkins 

[Deployment Manifest file](/images/01_create_deployment_file_for_jenkins.png)

Step 2: Create Service manifest file for Jenkins 

[Service Manifest file](/images/02_create_service_file_for_jenkins.png)

Step 3: Set app name as enviromental variable in the deployment stage 

[Environmental Variable App Name](/images/03_set_app_name_env_variable_in_deployment_stage.png)

Step 4: Add envsubst command in the deployment stage to enable it to replace environmental variables in the file through another copy of the file 

note: envsubst is used to substitute any variable defined inside a file, in this case it is a yaml file what this will do is that it will take the file given (envsubst < kubernetes/deployment.yaml) and it will look for syntax of $ and name of the variable and it will try to match that name of the variable to any environment variable defined in that context. It creates a temporary file with the values set and we are going to pipe that temporary file and pass it as a parameter  

[envsubst command in deployment stage](/images/04_add_envsubst_command_to_make_it_replace_env_variables_in_the_file_through_another_copy_of_the_file.png)

Step 5: Go into Jenkins container and install gettext-base as root user, this is the package that includes environmental substitute tool.

    apt-get install gettext-base

[Downloading gettext-base](/images/05_go_into_jenkins_container_and_install_gettext-base_as_root_user_this_is_the_package_that_includes_environment_substitute_tool.png)

[envsubst command available](/images/06_envsubst_command_available.png)

Step 6: Create docker secret on host 

    kubectl create secret docker-registry my-registry-key \
    --docker-server=docker.io \
    --docker-username=omacodes98 \
    --docker-password=xxxx 

[Creating docker secret on EKS cluster](/images/07_creating_docker_secret_on_host_with_three_values-server-username-password.png)

[Secret](/images/08_secret.png)

Step 7: Edit specification of pod in the manifest file by adding image pull secret 

[Image pull secret](/images/09_inside_specification_of_pod_set_image_pull_secrets.png)

Step 8: Build the Pipeline 

[Successful Pipeline](/images/10_successful_pipeline.png)

Step 9: Check if application was successfully deployed 

[Active pods](/images/11_active_pods_on_cluster.png)

## Installation

    apt-get install gettext-base

## Usage 

    envsubst < kubernetes/deployment.yaml | kubectl apply -f -
    envsubst < kubernetes/service.yaml | kubectl apply -f -

## How to Contribute

1. Clone the repo using $ git clone git@gitlab.com:omacodes98/complete-ci-cd-pipeline-with-eks-and-private-dockerhub-registry.git

2. Create a new branch $ git checkout -b your name 

3. Make Changes and test 

4. Submit a pull request with description for review



## Questions

Feel free to contact me for further questions via: 

Gitlab: https://gitlab.com/omacodes98

Email: omacodes98@gmail.com

## References

https://gitlab.com/omacodes98/complete-ci-cd-pipeline-with-eks-and-private-dockerhub-registry

## License

The MIT License 

For more informaation you can click the link below:

https://opensource.org/licenses/MIT

Copyright (c) 2022 Omatsola Beji.